CREATE TABLE users (
	id uuid NOT NULL,
	"name" varchar(255) NOT NULL,
	lastname varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	"password" varchar(255) NOT NULL DEFAULT ''::character varying,
	remember_password varchar(64) NOT NULL DEFAULT ''::character varying,
	CONSTRAINT users_pk PRIMARY KEY (id),
	CONSTRAINT users_un UNIQUE (email)
);

CREATE TABLE articles (
	id uuid NOT NULL,
	slug varchar(1024) NOT NULL,
	title varchar(512) NOT NULL,
	body varchar NOT NULL,
	image_path varchar(1024) NOT NULL,
	user_id uuid NOT NULL,
	article_date timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT articles_pk PRIMARY KEY (id),
	CONSTRAINT articles_un UNIQUE (slug),
	CONSTRAINT articles_user_fk FOREIGN KEY (user_id) REFERENCES users(id)
);
