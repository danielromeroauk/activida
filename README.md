# ActiVida

## Requirements

- GO 1.15
- PostgreSQL 13
- NodeJS 14.16.1 LTS
- Tailwindcss 2.1

## Correr la base de datos

```bash
make run-db
```

Para detener la base de datos:
```bash
docker-compose down
```

## Correr backend de GO

```bash
make run-backend
```

Para detener el backend se debe presionar la combinación de teclas `Ctrl+C`.

## API

Al correr GO nativamente el dominio es http://127.0.0.1:7020

### GET /articles

Permite los siguientes filtros a través de query params:
- slug
- user_id

Permite query params para paginación:
- page[number] => número de la página, por defecto es 1
- page[size] => cantidad de registros por página, por defecto es 6

Ejemplo:
```
GET http://127.0.0.1:7020/articles?page[number]=1&page[size]=6
```

#### Response 200
```json
{
  "data": [
    {
      "type": "articles",
      "id": "37a9ee6f-db76-429a-8c76-0adf1bc8fc66",
      "attributes": {
        "slug": "segunda-prueba",
        "title": "Daniel probando por segunda vez",
        "body": "Esta es un registro de la segunda prueba, este texto es el body de un registro de artículo.",
        "image_path": "https://gravatar.com/avatar/60ad9da6220cc102dd69a59d0f7285b5?s=100",
        "user_id": "b2f03e74-57ff-4eda-883e-fca6e1cb54f3",
        "article_date": "2021-05-02T17:37:08Z"
      },
      "links": {
        "self": "http://127.0.0.1:7020/articles/37a9ee6f-db76-429a-8c76-0adf1bc8fc66"
      }
    },
    {
      "type": "articles",
      "id": "1f119f2a-07c1-413e-bdac-461d0b6f1ca9",
      "attributes": {
        "slug": "primera-prueba",
        "title": "Daniel Prueba",
        "body": "Esta es un registro de prueba, este texto es el body de un registro de artículo.",
        "image_path": "https://gravatar.com/avatar/60ad9da6220cc102dd69a59d0f7285b5?s=100",
        "user_id": "b2f03e74-57ff-4eda-883e-fca6e1cb54f3",
        "article_date": "2021-05-01T20:31:25Z"
      },
      "links": {
        "self": "http://127.0.0.1:7020/articles/1f119f2a-07c1-413e-bdac-461d0b6f1ca9"
      }
    }
  ],
  "links": {
    "self": "http://127.0.0.1:7020/articles?page[number]=1&page[size]=6"
  }
}
```

### GET /articles/:id

`:id` es el UUID de un artículo.

Ejemplo:

```
GET http://127.0.0.1:7020/articles/1f119f2a-07c1-413e-bdac-461d0b6f1ca9
```

#### Response 200
```json
{
  "data": {
    "type": "articles",
    "id": "1f119f2a-07c1-413e-bdac-461d0b6f1ca9",
    "attributes": {
      "slug": "primera-prueba",
      "title": "Daniel Prueba",
      "body": "Esta es un registro de prueba, este texto es el body de un registro de artículo.",
      "image_path": "https://gravatar.com/avatar/60ad9da6220cc102dd69a59d0f7285b5?s=100",
      "user_id": "b2f03e74-57ff-4eda-883e-fca6e1cb54f3",
      "article_date": "2021-05-01T20:31:25Z"
    },
    "links": {
      "self": "http://127.0.0.1:7020/articles/1f119f2a-07c1-413e-bdac-461d0b6f1ca9"
    }
  }
}
```
