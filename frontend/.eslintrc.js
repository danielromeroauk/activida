module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true
  },
  extends: [
    "standard",
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  plugins: [
    "react"
  ],
  rules: {
    quotes: ["error", "double"],
    // "max-len": "warn",
    "react/prop-types": "warn",
    "space-before-function-paren": ["error", "never"],
    "react/jsx-closing-bracket-location": ["error", {
      selfClosing: "tag-aligned",
      nonEmpty: "tag-aligned"
    }],
    "react/jsx-max-props-per-line": ["error", { maximum: 1 }],
    "react/jsx-first-prop-new-line": ["error", "multiline"],
    "react/jsx-one-expression-per-line": ["error", { allow: "single-child" }]
  },
  settings: {
    react: {
      version: "latest"
    }
  }
}
