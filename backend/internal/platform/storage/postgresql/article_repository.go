package postgresql

import (
	"database/sql"
	"fmt"
	"strconv"

	"github.com/huandu/go-sqlbuilder"

	activida "danielromeroauk/activida/internal"
	"danielromeroauk/activida/kit/uuid"
)

// ArticleRepository is a Postgres activida.ArticleReaderInterface implementation
type ArticleRepository struct {
	db *sql.DB
}

// NewArticleRepository initializes a Postgres-based implementation
func NewArticleRepository(db *sql.DB) *ArticleRepository {
	return &ArticleRepository{
		db: db,
	}
}

// GetByID implements activida.ArticleReaderInterface
func (repo ArticleRepository) GetByID(id uuid.UUID) (*activida.Article, error) {
	articleSqlStruct := sqlbuilder.NewStruct(new(sqlArticle)).For(sqlbuilder.PostgreSQL)

	sb := articleSqlStruct.SelectFrom(sqlArticleTable)
	sb.Where(sb.Equal("id", id.String()))

	sqlQuery, args := sb.Build()
	fmt.Println("SQL Generado en postgresql.GetByID:", sqlQuery)
	fmt.Println(args)

	var row sqlArticle

	err := repo.db.QueryRow(sqlQuery, args...).Scan(articleSqlStruct.Addr(&row)...)
	if err != nil {
		return nil, err
	}

	return activida.NewArticle(
		row.ID,
		row.Slug,
		row.Title,
		row.Body,
		row.ImagePath,
		row.UserID,
		row.ArticleDate,
	)
}

// GetBySlug implements activida.ArticleReaderInterface
func (repo ArticleRepository) GetBySlug(slug activida.ArticleSlug) (*activida.Article, error) {
	articleSqlStruct := sqlbuilder.NewStruct(new(sqlArticle)).For(sqlbuilder.PostgreSQL)

	sb := articleSqlStruct.SelectFrom(sqlArticleTable)
	sb.Where(sb.Equal("slug", slug.String()))

	sqlQuery, args := sb.Build()
	fmt.Println("SQL Generado en postgresql.GetBySlug:", sqlQuery)
	fmt.Println(args)

	var row sqlArticle

	err := repo.db.QueryRow(sqlQuery, args...).Scan(articleSqlStruct.Addr(&row)...)
	if err != nil {
		return nil, err
	}

	return activida.NewArticle(
		row.ID,
		row.Slug,
		row.Title,
		row.Body,
		row.ImagePath,
		row.UserID,
		row.ArticleDate,
	)
}

func (repo ArticleRepository) GetRowsWithPagination(filters, sort, pagination map[string]string) ([]activida.Article, error) {
	articleSqlStruct := sqlbuilder.NewStruct(new(sqlArticle)).For(sqlbuilder.PostgreSQL)

	sb := articleSqlStruct.SelectFrom(sqlArticleTable)

	for field, value := range filters {
		if value == "" {
			continue
		}

		sb.Where(sb.Equal(field, value))
	}

	for field, value := range sort {
		if value == "desc" {
			sb.OrderBy(field).Desc()
		} else {
			sb.OrderBy(field).Asc()
		}
	}

	if value, ok := pagination["limit"]; ok && value != "" {
		limit, err := strconv.Atoi(value)
		if err != nil {
			return nil, fmt.Errorf("convirtiendo a int pagination limit: %v", err)
		}
		sb.Limit(limit)
	}

	if value, ok := pagination["offset"]; ok && value != "" {
		offset, err := strconv.Atoi(value)
		if err != nil {
			return nil, fmt.Errorf("convirtiendo a int pagination offset: %v", err)
		}
		sb.Offset(offset)
	}

	sqlQuery, args := sb.Build()
	fmt.Println("SQL Generado en postgresql.GetBySlug:", sqlQuery)
	fmt.Println(args)

	dbRows, err := repo.db.Query(sqlQuery, args...)
	if err != nil {
		return nil, fmt.Errorf("ejecutando SQL en GetAllWithPagination: %v", err)
	}
	defer dbRows.Close()

	articles := []activida.Article{}

	for dbRows.Next() {
		row := sqlArticle{}
		err := dbRows.Scan(articleSqlStruct.Addr(&row)...)
		if err != nil {
			return nil, fmt.Errorf("leyendo dbRow en GetAllWithPagination: %v", err)
		}

		article, err := activida.NewArticle(
			row.ID,
			row.Slug,
			row.Title,
			row.Body,
			row.ImagePath,
			row.UserID,
			row.ArticleDate,
		)
		if err != nil {
			return nil, fmt.Errorf("instanciando activida.NewArticle en GetAllWithPagination: %v", err)
		}

		articles = append(articles, *article)
	}

	return articles, nil
}
