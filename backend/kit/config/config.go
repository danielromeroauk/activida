package config

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
)

const Prefix = "ACTIVIDA"

type Config struct {
	// Server configuration
	Domain string `required:"true"`
	Host   string `required:"true"`
	Port   uint16 `required:"true"`
	// Database configuration
	Db struct {
		Host     string `required:"true"`
		User     string `required:"true"`
		Password string `required:"true"`
		Port     uint   `required:"true"`
		Name     string `required:"true"`
	}
}

func NewConfig() (*Config, error) {
	var cfg Config

	err := envconfig.Process(Prefix, &cfg)
	if err != nil {
		return nil, fmt.Errorf("leyendo variables de entorno: %v", err)
	}

	return &cfg, nil
}
