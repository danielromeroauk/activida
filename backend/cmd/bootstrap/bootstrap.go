package bootstrap

import (
	"danielromeroauk/activida/internal/platform/server"
	"danielromeroauk/activida/kit/config"
)

func Run() error {
	cfg, err := config.NewConfig()
	if err != nil {
		return err
	}

	svr := server.New(cfg.Host, cfg.Port)

	return svr.Run()
}
