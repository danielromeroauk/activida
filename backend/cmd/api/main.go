package main

import (
	"log"

	"danielromeroauk/activida/cmd/bootstrap"
)

func main() {
	if err := bootstrap.Run(); err != nil {
		log.Fatal(err)
	}
}
