.PHONY: all help

# Abbreviated git commit sha
COMMIT_SHA := dev-$(shell git rev-parse --short HEAD)

ifneq (,$(wildcard ./.env))
    include .env
    export
endif

# target: all - Default target. No hace nada
all:
	@echo "Hola $(LOGNAME), nada que hacer por default."
	@echo "Intenta 'make help'"


# target: help - Muestra los targets
help:
	@grep -E "^# target:" [Mm]akefile


# target: run-db
run-db:
	docker-compose up -d


# target: run-backend - Compila y corre el backend de GO.
run-backend:
	cd backend/cmd/api && go run main.go
