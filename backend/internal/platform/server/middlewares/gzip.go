package middlewares

import (
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Gzip() echo.MiddlewareFunc {
	return middleware.GzipWithConfig(
		middleware.GzipConfig{
			Level: 5,
			Skipper: func(c echo.Context) bool {
				// no comprime si está accediendo a GraphiQL con GET
				// requerido para usar GraphiQL
				return c.Request().Method == "GET" &&
					strings.HasPrefix(c.Request().RequestURI, "/go/graphql")
			},
		},
	)
}
