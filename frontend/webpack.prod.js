const merge = require("webpack-merge")
const common = require("./webpack.common")
const Dotenv = require("dotenv-webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = merge(common, {
  mode: "production",

  devtool: "source-map",

  plugins: [
    new Dotenv({ path: "./.env" }),

    new HtmlWebpackPlugin({
      template: "src/index.html",
      minify: true
    })
  ],

  externals: {
    react: "React",
    "react-dom": "ReactDOM",
    axios: "axios"
  }
})
