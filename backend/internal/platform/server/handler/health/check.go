package health

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// CheckHandler returns an HTTP handler to perform health checks.
func CheckHandler(c echo.Context) error {
	return c.String(http.StatusOK, "ok")
}
