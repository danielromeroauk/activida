package reading

import (
	activida "danielromeroauk/activida/internal"
	"danielromeroauk/activida/kit/uuid"
)

// ArticleReader default implementation interface activida.ArticleReaderInterface
type ArticleReader struct {
	reading activida.ArticleReaderInterface
}

// NewArticleReader is the constructor
func NewArticleReader(reading activida.ArticleReaderInterface) ArticleReader {
	return ArticleReader{
		reading: reading,
	}
}

// GetBySlug implements activida.ArticleReaderInterface
func (r ArticleReader) GetBySlug(slug activida.ArticleSlug) (*activida.Article, error) {
	return r.reading.GetBySlug(slug)
}

// GetByID implements activida.ArticleReaderInterface
func (r ArticleReader) GetByID(id uuid.UUID) (*activida.Article, error) {
	return r.reading.GetByID(id)
}

// GetRowsWithPagination implements activida.ArticleReaderInterface
func (r ArticleReader) GetRowsWithPagination(filters, sort, pagination map[string]string) ([]activida.Article, error) {
	return r.reading.GetRowsWithPagination(filters, sort, pagination)
}
