package activida

import (
	"errors"

	"danielromeroauk/activida/kit/datetime"
	"danielromeroauk/activida/kit/uuid"
)

var ErrLenArticleSlug = errors.New("the length of field ArticleSlug can not be greater than 1024 characters")
var ErrEmptyArticleSlug = errors.New("the field ArticleSlug can not be empty")

// ArticleSlug represents the article slug
type ArticleSlug struct {
	value string
}

// NewArticleSlug instantiate the VO for ArticleSlug
func NewArticleSlug(value string) (ArticleSlug, error) {
	if value == "" {
		return ArticleSlug{}, ErrEmptyArticleSlug
	}

	if len(value) > 1024 {
		return ArticleSlug{}, ErrLenArticleSlug
	}

	return ArticleSlug{
		value: value,
	}, nil
}

// String type converts the ArticleSlug into string
func (slug ArticleSlug) String() string {
	return slug.value
}

var ErrLenArticleTitle = errors.New("the length of field ArticleTitle can not be greater than 512 characters")
var ErrEmptyArticleTitle = errors.New("the field ArticleTitle can not be empty")

// ArticleTitle represents the article title
type ArticleTitle struct {
	value string
}

// NewArticleTitle instantiate the VO for ArticleTitle
func NewArticleTitle(value string) (ArticleTitle, error) {
	if value == "" {
		return ArticleTitle{}, ErrEmptyArticleTitle
	}

	if len(value) > 512 {
		return ArticleTitle{}, ErrLenArticleTitle
	}

	return ArticleTitle{
		value: value,
	}, nil
}

// String type converts the ArticleTitle into string
func (title ArticleTitle) String() string {
	return title.value
}

var ErrEmptyArticleBody = errors.New("the field ArticleBody can not be empty")

// ArticleBody represents the article body
type ArticleBody struct {
	value string
}

// NewArticleBody instantiate the VO for ArticleBody
func NewArticleBody(value string) (ArticleBody, error) {
	if value == "" {
		return ArticleBody{}, ErrEmptyArticleBody
	}

	return ArticleBody{
		value: value,
	}, nil
}

// String type converts the ArticleBody into string
func (body ArticleBody) String() string {
	return body.value
}

var ErrLenArticleImagePath = errors.New("the length of field ArticleImagePath can not be greater than 1024 characters")
var ErrEmptyArticleImagePath = errors.New("the field ArticleImagePath can not be empty")

// ArticleImagePath represents the article imagePath
type ArticleImagePath struct {
	value string
}

// NewArticleImagePath instantiate the VO for ArticleImagePath
func NewArticleImagePath(value string) (ArticleImagePath, error) {
	if value == "" {
		return ArticleImagePath{}, ErrEmptyArticleImagePath
	}

	if len(value) > 1024 {
		return ArticleImagePath{}, ErrLenArticleImagePath
	}

	return ArticleImagePath{
		value: value,
	}, nil
}

// String type converts the ArticleImagePath into string
func (imagePath ArticleImagePath) String() string {
	return imagePath.value
}

// Article is the data structure that represents a article
type Article struct {
	ID          uuid.UUID
	Slug        ArticleSlug
	Title       ArticleTitle
	Body        ArticleBody
	ImagePath   ArticleImagePath
	UserID      uuid.UUID
	ArticleDate datetime.DateTime
}

// ArticleReadingInterface defines the expected behaviour from a article storage
type ArticleReaderInterface interface {
	GetByID(id uuid.UUID) (*Article, error)
	GetBySlug(slug ArticleSlug) (*Article, error)
	GetRowsWithPagination(filters, sort, pagination map[string]string) ([]Article, error)
}

//go:generate mockery --case=snake --outpkg=storagemocks --output=platform/storage/storagemocks --name=ArticleRepository

func NewArticle(id, slug, title, body, imagePath, userID, articleDate string) (*Article, error) {
	idVO, err := uuid.NewUUID(id)
	if err != nil {
		return nil, err
	}

	slugVO, err := NewArticleSlug(slug)
	if err != nil {
		return nil, err
	}

	titleVO, err := NewArticleTitle(title)
	if err != nil {
		return nil, err
	}

	bodyVO, err := NewArticleBody(body)
	if err != nil {
		return nil, err
	}

	imagePathVO, err := NewArticleImagePath(imagePath)
	if err != nil {
		return nil, err
	}

	userIDVO, err := uuid.NewUUID(userID)
	if err != nil {
		return nil, err
	}

	articleDateV0, err := datetime.NewDateTimeFromString(articleDate)
	if err != nil {
		return nil, err
	}

	return &Article{
		ID:          idVO,
		Slug:        slugVO,
		Title:       titleVO,
		Body:        bodyVO,
		ImagePath:   imagePathVO,
		UserID:      userIDVO,
		ArticleDate: articleDateV0,
	}, nil
}
