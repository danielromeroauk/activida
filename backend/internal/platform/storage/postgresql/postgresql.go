package postgresql

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"

	"danielromeroauk/activida/kit/config"
)

var connectionPool *sql.DB

func GetConnection() (*sql.DB, error) {
	if connectionPool != nil {
		err := connectionPool.Ping()
		if err == nil {
			return connectionPool, nil
		}
	}

	cfg, err := config.NewConfig()
	if err != nil {
		return nil, err
	}

	connectionPool, err := sql.Open(
		"postgres",
		fmt.Sprintf(
			"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			cfg.Db.Host,
			cfg.Db.Port,
			cfg.Db.User,
			cfg.Db.Password,
			cfg.Db.Name,
		),
	)
	if err != nil {
		return nil, err
	}

	err = connectionPool.Ping()
	return connectionPool, err
}
