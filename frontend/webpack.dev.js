const webpack = require("webpack")
const path = require("path")
const { merge } = require("webpack-merge")
const common = require("./webpack.common")
const Dotenv = require("dotenv-webpack")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = merge(common, {
  mode: "development",

  devtool: "inline-source-map",

  plugins: [
    new Dotenv({ path: "./.env.development" }),

    new HtmlWebpackPlugin({
      template: "src/index.local.html",
      minify: true
    }),

    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
      cleanOnceBeforeBuildPatterns: [], // leave this as an empty array
      cleanAfterEveryBuildPatterns: [
        "*.js*",
        "!modules-manifest.json",
        "!modules.js"
      ]
    }),

    new webpack.DllReferencePlugin({
      manifest: require("./static/modules-manifest.json")
    })
  ],

  devServer: {
    open: true,
    contentBase: path.join(__dirname, "static"),
    publicPath: "/",
    contentBasePublicPath: "/static",
    hot: true, // agrega o remueve módulos en caliente
    hotOnly: true, // para que el navegador no recargue la página
    writeToDisk: true,
    historyApiFallback: true,
    clientLogLevel: "debug",
    disableHostCheck: true,
    overlay: {
      warnings: true,
      errors: true
    },
    progress: true,
    stats: "errors-warnings"
    // proxy: {
    //   "/api": "http://127.0.0.1:2080",
    //   "/graphql": "http://127.0.0.1:2080",
    //   "/go/payroll": "http://127.0.0.1:2031",
    //   "/go": "http://127.0.0.1:2022"
    // }
  },

  watchOptions: {
    aggregateTimeout: 200,
    poll: true,
    ignored: /node_modules/
  }
})
