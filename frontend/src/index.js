/* eslint-disable react/jsx-max-props-per-line */
import React from "react"
import ReactDOM from "react-dom"
import { createStore } from "redux"
import { Provider } from "react-redux"
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom"

import { NotFoundPage } from "./pages/NotFound/NotFound"
import { HomePage } from "./pages/Home/Home"

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage} />

        {/* Catch-all */}
        <Route path="*" component={NotFoundPage} />
      </Switch>
    </Router>
  )
}

const store = createStore(() => {})

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
)
