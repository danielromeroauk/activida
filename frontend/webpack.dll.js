const webpack = require("webpack")
const path = require("path")

module.exports = {
  mode: "development",

  entry: {
    modules: ["react", "react-dom", "axios"]
  },

  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "static"),
    library: "[name]"
  },

  plugins: [
    new webpack.DllPlugin({
      name: "[name]",
      path: path.join(__dirname, "./static/[name]-manifest.json")
    })
  ]
}
