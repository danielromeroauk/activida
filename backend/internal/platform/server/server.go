package server

import (
	"danielromeroauk/activida/internal/platform/server/handler/articles"
	"danielromeroauk/activida/internal/platform/server/middlewares"
	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"danielromeroauk/activida/internal/platform/server/handler/health"
)

type Server struct {
	httpAddress string
	engine      *echo.Echo
}

func New(host string, port uint16) Server {
	svr := Server{
		httpAddress: fmt.Sprintf("%s:%d", host, port),
		engine:      echo.New(),
	}

	svr.registerMiddlewares()
	svr.registerRoutes()

	return svr
}

func (s *Server) registerMiddlewares() {
	s.engine.Use(middleware.Recover())
	s.engine.Use(middlewares.Cors())
	s.engine.Use(middlewares.Gzip())
	s.engine.Pre(middleware.RemoveTrailingSlash())
}

func (s *Server) registerRoutes() {
	s.engine.GET("/health", health.CheckHandler)
	s.engine.GET("/articles", articles.GetArticlesHandler)
	s.engine.GET("/articles/:id", articles.GetArticleByIDHandler)
}

func (s *Server) Run() error {
	return s.engine.Start(s.httpAddress)
}
