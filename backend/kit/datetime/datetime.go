package datetime

import (
	"time"
)

const DATETIME = "2006-01-02 15:04:05"

// DateTime represents a timestamp
type DateTime struct {
	value time.Time
}

// NewDateTimeFromString instantiate the VO for DateTime
func NewDateTime(value time.Time) DateTime {
	return DateTime{
		value: value,
	}
}

// NewDateTimeFromString instantiate the VO for DateTime using string value
// returns error if hours, minutes or seconds does not presents
func NewDateTimeFromString(value string) (DateTime, error) {
	dt, err := time.Parse(time.RFC3339, value)
	if err != nil {
		return DateTime{}, err
	}

	return DateTime{
		value: dt.In(time.UTC),
	}, nil
}

// String retorna un string con el formato yyyy-mm-dd hh:ii:ss
func (dt DateTime) String() string {
	return dt.value.Format(time.RFC3339)
}
