package httpresponse

import (
	"log"

	"github.com/labstack/echo/v4"
)

// EchoMessageResponse retorna un mensaje de tipo JSON con echo
// si statusCode es mayor o igual a 400 imprime log del mensaje
func EchoMessageResponse(c echo.Context, statusCode int, message string) error {
	if statusCode >= 400 {
		log.Println(message)
		return echo.NewHTTPError(statusCode, message)
	}

	return c.JSON(statusCode, map[string]string{
		"message": message,
	})
}
