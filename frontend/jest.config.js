module.exports = {
  moduleNameMapper: {
    "\\.svg$": "<rootDir>/__mocks__/svgrMock.js"
  },
  // setupFilesAfterEnv: [
  //   "<rootDir>/src/setuptests.ts"
  // ],
  restoreMocks: true,
  setupFiles: [
    "<rootDir>/__mocks__/react-select.js"
  ],
  testTimeout: 30000
}
