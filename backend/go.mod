module danielromeroauk/activida

go 1.15

require (
	github.com/google/uuid v1.2.0
	github.com/huandu/go-sqlbuilder v1.12.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo/v4 v4.2.2
	github.com/lib/pq v1.10.1
)
