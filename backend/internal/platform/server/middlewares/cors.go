package middlewares

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// Cors contiene la configuración para peticiones desde otros dominios
func Cors() echo.MiddlewareFunc {
	return middleware.CORSWithConfig(
		middleware.CORSConfig{
			Skipper: middleware.DefaultSkipper,

			AllowHeaders: []string{"*"},

			AllowOrigins: []string{"*"},

			AllowMethods: []string{
				http.MethodGet,
				http.MethodPut,
				http.MethodPost,
				http.MethodOptions,
			},
		},
	)
}
