package articles

import (
	"fmt"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"

	"danielromeroauk/activida/internal/platform/storage/postgresql"
	"danielromeroauk/activida/internal/reading"
	"danielromeroauk/activida/kit/config"
	"danielromeroauk/activida/kit/httpresponse"
	"danielromeroauk/activida/kit/uuid"
)

type Links struct {
	Self  string `json:"self,omitempty"`
	First string `json:"first,omitempty"`
	Prev  string `json:"prev,omitempty"`
	Next  string `json:"next,omitempty"`
	Last  string `json:"last,omitempty"`
}

type ArticleAttributes struct {
	Slug        string `json:"slug" query:"slug"`
	Title       string `json:"title" query:"title"`
	Body        string `json:"body" query:"body"`
	ImagePath   string `json:"image_path" query:"image_path"`
	UserID      string `json:"user_id" query:"user_id"`
	ArticleDate string `json:"article_date" query:"article_date"`
}

type ArticleData struct {
	Type       string            `json:"type"`
	ID         string            `json:"id"`
	Attributes ArticleAttributes `json:"attributes"`
	Links      Links             `json:"links"`
}

type ApiResponse struct {
	Data  []ArticleData `json:"data"`
	Links Links         `json:"links"`
}

type Page struct {
	Size   uint64 `json:"size" query:"page[size]"`
	Number uint64 `json:"number" query:"page[number]"`
}

// GetArticlesHandler returns an HTTP handler with articles
func GetArticlesHandler(c echo.Context) error {
	cfg, err := config.NewConfig()
	if err != nil {
		return httpresponse.EchoMessageResponse(c, http.StatusInternalServerError, err.Error())
	}

	page := Page{
		Size:   6,
		Number: 1,
	}
	if err := (&echo.DefaultBinder{}).BindQueryParams(c, &page); err != nil {
		return httpresponse.EchoMessageResponse(c, http.StatusBadRequest, err.Error())
	}

	var filters ArticleAttributes
	if err := (&echo.DefaultBinder{}).BindQueryParams(c, &filters); err != nil {
		return httpresponse.EchoMessageResponse(c, http.StatusBadRequest, err.Error())
	}

	db, err := postgresql.GetConnection()
	if err != nil {
		log.Printf("conectando con postgres: %v\n", err)
		return httpresponse.EchoMessageResponse(c, http.StatusInternalServerError, "Error conectando con la base de datos")
	}

	repo := postgresql.NewArticleRepository(db)
	reader := reading.NewArticleReader(repo)

	articles, err := reader.GetRowsWithPagination(
		map[string]string{
			"slug":    filters.Slug,
			"user_id": filters.UserID,
		},
		map[string]string{"article_date": "desc"},
		map[string]string{
			"limit":  fmt.Sprintf("%d", page.Size),
			"offset": fmt.Sprintf("%d", page.Size*(page.Number-1)),
		},
	)
	if err != nil {
		return httpresponse.EchoMessageResponse(c, http.StatusInternalServerError, err.Error())
	}

	data := []ArticleData{}
	links := Links{}

	for _, article := range articles {
		attributes := ArticleAttributes{
			Slug:        article.Slug.String(),
			Title:       article.Title.String(),
			Body:        article.Body.String(),
			ImagePath:   article.ImagePath.String(),
			UserID:      article.UserID.String(),
			ArticleDate: article.ArticleDate.String(),
		}

		links.Self = fmt.Sprintf(
			"%s:%d/articles/%s",
			cfg.Domain,
			cfg.Port,
			article.ID.String(),
		)

		articleData := ArticleData{
			Type:       "articles",
			ID:         article.ID.String(),
			Attributes: attributes,
			Links:      links,
		}

		data = append(data, articleData)
	}

	links.Self = fmt.Sprintf(
		"%s:%d/articles?page[number]=%d&page[size]=%d",
		cfg.Domain,
		cfg.Port,
		page.Number,
		page.Size,
	)

	var nextPage uint64
	if uint64(len(data)) == page.Size {
		nextPage = page.Number + 1
	}

	if nextPage > 0 {
		links.Next = fmt.Sprintf(
			"%s:%d/articles?page[number]=%d&page[size]=%d",
			cfg.Domain,
			cfg.Port,
			nextPage,
			page.Size,
		)
	}

	if page.Number > 1 {
		links.Prev = fmt.Sprintf(
			"%s:%d/articles?page[number]=%d&page[size]=%d",
			cfg.Domain,
			cfg.Port,
			page.Number-1,
			page.Size,
		)
	}

	resp := ApiResponse{
		Data:  data,
		Links: links,
	}

	return c.JSON(http.StatusOK, resp)
}

// GetArticleByIDHandler returns an HTTP handler with articles
func GetArticleByIDHandler(c echo.Context) error {
	cfg, err := config.NewConfig()
	if err != nil {
		return httpresponse.EchoMessageResponse(c, http.StatusInternalServerError, err.Error())
	}

	idVO, err := uuid.NewUUID(c.Param("id"))
	if err != nil {
		return httpresponse.EchoMessageResponse(c, http.StatusInternalServerError, err.Error())
	}

	db, err := postgresql.GetConnection()
	if err != nil {
		log.Printf("conectando con postgres: %v\n", err)
		return httpresponse.EchoMessageResponse(c, http.StatusInternalServerError, "Error conectando con la base de datos")
	}

	repo := postgresql.NewArticleRepository(db)
	reader := reading.NewArticleReader(repo)

	article, err := reader.GetByID(idVO)
	if err != nil {
		return httpresponse.EchoMessageResponse(c, http.StatusInternalServerError, err.Error())
	}

	links := Links{}

	attributes := ArticleAttributes{
		Slug:        article.Slug.String(),
		Title:       article.Title.String(),
		Body:        article.Body.String(),
		ImagePath:   article.ImagePath.String(),
		UserID:      article.UserID.String(),
		ArticleDate: article.ArticleDate.String(),
	}

	links.Self = fmt.Sprintf(
		"%s:%d/articles/%s",
		cfg.Domain,
		cfg.Port,
		article.ID.String(),
	)

	articleData := ArticleData{
		Type:       "articles",
		ID:         article.ID.String(),
		Attributes: attributes,
		Links:      links,
	}

	resp := map[string]interface{}{
		"data": articleData,
	}

	return c.JSON(http.StatusOK, resp)
}
