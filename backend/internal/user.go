package activida

import (
	"errors"

	"danielromeroauk/activida/kit/email"
	"danielromeroauk/activida/kit/uuid"
)

var ErrLenUserName = errors.New("the length of field UserName can not be greater than 255 characters")
var ErrEmptyUserName = errors.New("the field UserName can not be empty")

// UserName represents the user name
type UserName struct {
	value string
}

// NewUserName instantiate the VO for UserName
func NewUserName(value string) (UserName, error) {
	if value == "" {
		return UserName{}, ErrEmptyUserName
	}

	if len(value) > 255 {
		return UserName{}, ErrLenUserName
	}

	return UserName{
		value: value,
	}, nil
}

// String type converts the UserName into string
func (name UserName) String() string {
	return name.value
}

var ErrLenUserLastName = errors.New("the length of field UserLastName can not be greater than 255 characters")
var ErrEmptyUserLastName = errors.New("the field UserLastName can not be empty")

// UserLastName represents the user lastname
type UserLastName struct {
	value string
}

// NewUserLastName instantiate the VO for UserLastName
func NewUserLastName(value string) (UserLastName, error) {
	if value == "" {
		return UserLastName{}, ErrEmptyUserLastName
	}

	if len(value) > 255 {
		return UserLastName{}, ErrLenUserLastName
	}

	return UserLastName{
		value: value,
	}, nil
}

// String type converts the UserLastName into string
func (lastname UserLastName) String() string {
	return lastname.value
}

var ErrInvalidEmail = errors.New("the field UserEmail is invalid")
var ErrEmptyUserEmail = errors.New("the field UserEmail can not be empty")

// UserEmail represents the user email
type UserEmail struct {
	value string
}

// NewUserEmail instantiate the VO for UserEmail.
// It also checks the domain has a valid MX record
func NewUserEmail(value string) (UserEmail, error) {
	if value == "" {
		return UserEmail{}, ErrEmptyUserEmail
	}

	if !email.IsEmailValid(value) {
		return UserEmail{}, ErrInvalidEmail
	}

	return UserEmail{
		value: value,
	}, nil
}

// String type converts the UserEmail into string
func (email UserEmail) String() string {
	return email.value
}

var ErrLenUserPassword = errors.New("the length of field UserPassword can not be greater than 255 characters")
var ErrEmptyUserPassword = errors.New("the field UserPassword can not be empty")

// UserPassword represents the user password
type UserPassword struct {
	value string
}

// NewUserPassword instantiate the VO for UserPassword
func NewUserPassword(value string) (UserPassword, error) {
	if value == "" {
		return UserPassword{}, ErrEmptyUserPassword
	}

	if len(value) > 255 {
		return UserPassword{}, ErrLenUserPassword
	}

	return UserPassword{
		value: value,
	}, nil
}

// String type converts the UserPassword into string
func (password UserPassword) String() string {
	return password.value
}

var ErrLenUserRememberPassword = errors.New("the length of field UserRememberPassword can not be greater than 64 characters")
var ErrEmptyUserRememberPassword = errors.New("the field UserRememberPassword can not be empty")

// UserRememberPassword represents the user rememberPassword
type UserRememberPassword struct {
	value string
}

// NewUserRememberPassword instantiate the VO for UserRememberPassword
func NewUserRememberPassword(value string) (UserRememberPassword, error) {
	if len(value) > 64 {
		return UserRememberPassword{}, ErrLenUserRememberPassword
	}

	return UserRememberPassword{
		value: value,
	}, nil
}

// String type converts the UserRememberPassword into string
func (rememberPassword UserRememberPassword) String() string {
	return rememberPassword.value
}

// User is the data structure that represents a user
type User struct {
	ID               uuid.UUID
	Name             UserName
	LastName         UserLastName
	Email            UserEmail
	Password         UserPassword
	RememberPassword UserRememberPassword
}
