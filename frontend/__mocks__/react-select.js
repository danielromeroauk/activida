import React from "react"

jest.mock("react-select", () => ({ options, value, onChange, testid }) => {
  function handleChange(event) {
    const option = options.find(
      (option) => option.value === event.currentTarget.value
    )
    onChange(option)
  }
  return (
    <select
      data-testid={testid}
      value={value}
      onChange={handleChange}
    >
      {options.map(({ label, value }, idx) => (
        <option
          key={idx}
          value={value}
        >
          {label}
        </option>
      ))}
    </select>
  )
})
