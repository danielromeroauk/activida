package email

import (
	"testing"
)

func Test_isEmailValid(t *testing.T) {
	if email := "operaciones@finanzas-simples.com"; !isEmailValid(email) {
		t.Errorf("%s is not a valid email", email)
	}

	if e := "test@google.com"; !isEmailValid(e) {
		t.Errorf(e + " not a valid email")
	}
}
