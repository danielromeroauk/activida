const webpack = require("webpack")
const path = require("path")
const CopyWebpackPlugin = require("copy-webpack-plugin")

module.exports = {
  entry: {
    main: "./src/index.js"
  },

  output: {
    filename: "[name].[hash].bundle.js",
    path: path.resolve(__dirname, "static"),
    publicPath: "/static/"
  },

  optimization: {
    splitChunks: {
      chunks: "async"
    }
  },

  plugins: [
    new CopyWebpackPlugin(
      {
        patterns: [
          { from: "./src/assets/", to: "assets/" }
        ]
      }
    )
  ],

  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.svg$/,
        use: ["@svgr/webpack"]
      }
    ]
  },

  resolve: {
    extensions: [".js", ".json"]
  }
}
