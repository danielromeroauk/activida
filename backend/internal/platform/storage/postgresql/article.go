package postgresql

const (
	sqlArticleTable = "articles"
)

type sqlArticle struct {
	ID          string `db:"id"`
	Slug        string `db:"slug"`
	Title       string `db:"title"`
	Body        string `db:"body"`
	ImagePath   string `db:"image_path"`
	UserID      string `db:"user_id"`
	ArticleDate string `db:"article_date"`
}
